package com.artcon.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.artcon.model.User;

@Repository
public class UserDaoImp implements UserDao{
	private static final Logger logger = LoggerFactory.getLogger(UserDaoImp.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(user);
		logger.info("Person saved successfully, Person Details="+user);
	}
	@Override
	public User getUserById(String email) {
		Session session = this.sessionFactory.getCurrentSession();		
		User p = (User) session.get(User.class, new String(email));
		logger.info("Person loaded successfully, Person details="+p);
		return p;
	}

/*	@Override
	public void updatePerson(Person p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Person updated successfully, Person Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> listPersons() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Person> personsList = session.createQuery("from Person").list();
		for(Person p : personsList){
			logger.info("Person List::"+p);
		}
		return personsList;
	}

	

	@Override
	public void removePerson(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Person p = (Person) session.load(Person.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Person deleted successfully, person details="+p);
	}*/
	@Override
	public List<User> getUserByName(String name){
		
		logger.info("Inside getUserByName");
		Session session = this.sessionFactory.getCurrentSession();
		List<User> userList = session.createQuery("from User  where fName like :name").setParameter("name", "%" + name + "%").list();
		for(User u : userList){  
			logger.info("User List::"+u);
		}
		return userList;
	}

}


