package com.artcon.dao;

import java.util.List;

import com.artcon.model.User;

public interface UserDao {

	public void addUser(User user);
	public User getUserById(String id);
	/*public void updatePerson(User p);
	public List<User> listUsers();
	
	public void removeUser(int id);*/
	public List<User> getUserByName(String name);
}
