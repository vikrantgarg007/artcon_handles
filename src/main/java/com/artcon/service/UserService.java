package com.artcon.service;

import java.util.List;

import com.artcon.model.User;

public interface UserService {

	public void addUser(User p);
	public User getUserById(String id);
	public String passwordEncription(String password);
	/*public void updateUser(User p);
	public List<User> listUsers();
	
	public void removeUser(int id);*/
	public List<User> getUserByName(String name);

	
}
