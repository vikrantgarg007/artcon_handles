package com.artcon.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.artcon.dao.UserDao;
import com.artcon.model.User;

@Service
public class UserServiceImp implements UserService {

	private UserDao userDao;
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	@Transactional
	public void addUser(User p) {
	p.setPassword(passwordEncription(p.getPassword()));	 
	this.userDao.addUser(p);
	}
	
	@Override
	@Transactional
	public User getUserById(String id) {
		return this.userDao.getUserById(id) ;
	}
	
/*	@Override
	@Transactional
	public void updatePerson(Person p) {
		this.personDAO.updatePerson(p);
	}

	@Override
	@Transactional
	public List<Person> listPersons() {
		return this.personDAO.listPersons();
	}

	

	@Override
	@Transactional
	public void removePerson(int id) {
		this.personDAO.removePerson(id);
	}*/
	@Override
	public String passwordEncription(String password) 
    {
        String passwordToHash = password;
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }
	@Override
	@Transactional
	public List<User> getUserByName(String name){
		return this.userDao.getUserByName(name);
	}
}
