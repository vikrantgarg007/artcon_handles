package com.artcon.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.artcon.model.User;
import com.artcon.service.UserService;

/**
 * Handles requests for the application home page.
 */
@SessionAttributes("user")
@Controller
public class HomeController {

/*	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);
*/
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		//logger.info("Welcome home!  {}.", locale);

		

		return "index";
	}

	@RequestMapping(value = "/signIn", method = RequestMethod.GET)
	public String signIn() {
		return "SignIn";

	}

	@ModelAttribute("typeOptions")
	public List<String> getTypes() {
		System.out.println("Adding typeOptions");
		return new LinkedList<String>(Arrays.asList(new String[] { "ARTIST",
				"FAN", "EMPLOYER"}));
	}
	
	@ModelAttribute("locationOptions")
	public List<String> getLocation() {
		System.out.println("Adding locationOptions");
		return new LinkedList<String>(Arrays.asList(new String[] { "New-Delhi",
				"Noida", "Gurgaon"}));
	}
	@ModelAttribute("user")
	public User getUser() {
		System.out.println("Adding a new user to the model");
		return new User();
	}

	@ModelAttribute("gender")
	public List<String> getGender() {
		return new LinkedList<String>(Arrays.asList(new String[] { "MALE",
				"FEMALE" }));
	}

	@RequestMapping("/home")
	public String myhome() {
		return "home";
	}

private UserService userService;
	
	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setUserService(UserService user){
		this.userService = user;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute User user) {
		
		/*Checking if the user exists or not i.e. email id exists or not
		 * returns the object if the user exists otherwise null object
		 */
		User user2 = this.userService.getUserById(user.getEmail());

			//If email Id not found then add the user
			if(user2 == null){
			this.userService.addUser(user);
			System.out.println(user);
			return "success";
			}
			else{
			return "emailidexists";
			}
				
		/*else{
			//existing person, call update
			this.personService.updatePerson(p);
		}*/
		
	}
	/*
	 * Closing the session
	 */
	@RequestMapping(value = "/logout")
	public String successLogOut(@ModelAttribute User user, HttpSession session, SessionStatus status){
		
		 status.setComplete();
	     session.removeAttribute("user");
		return "redirect:/home";
	}
	
	//redirects to the login Page
	@RequestMapping("/login")
	public String logIn(){
		return "login";
	}
	
	//User enters the submit button of loginIn form
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public String successLogIn(@ModelAttribute User user,HttpSession session, SessionStatus status){
		
		/*Checking if the user exists or not i.e. email id exists or not
		 * returns the object if the user exists otherwise null
		 */
		User user2 = this.userService.getUserById(user.getEmail());
		
		//Encripting  entered login Password
		user.setPassword(this.userService.passwordEncription(user.getPassword()));
		
		//If the encripted password matches the input password
		if(user2 != null && user2.getPassword().equals(user.getPassword())){
			return "success";
		}
		else{
			status.setComplete();
		    session.removeAttribute("user");  //closing session and returning home
			return "redirect:/home";
		}
	}
	@RequestMapping(value = "/search",method = RequestMethod.GET)
	public String search(){
		
		return "search";
	}
	@RequestMapping(value = "/search",method = RequestMethod.POST)
	public String searchResult(@ModelAttribute User user,Model model){
		List<User> list = new ArrayList<User>();
		list = this.userService.getUserByName(user.getfName());
		model.addAttribute("users",list);
		return "searchResult";
	}
}
