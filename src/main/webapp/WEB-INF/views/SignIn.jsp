<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Manager</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="<spring:url value="/resources/css/home.css"/>" type="text/css"/>
	<link rel="stylesheet" href="<spring:url value="/resources/css/bootstrap-select.min.css"/>" type="text/css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="<spring:url value="/resources/js/bootstrap-select.min.js"/>"></script>

</head>
<body>
	
	<div class="container">	
		<div class="row">
			<spring:url value="/save" var="formUrl"/>
			<form:form modelAttribute="user" action="${formUrl }" method="post" cssClass="col-md-8 col-md-offset-2">
			
				<div class="form-group">
					<label for="user-name">First Name</label>
					<form:input id="user-name" 
							cssClass="form-control" path="fName"/>
				</div>
				<div class="form-group">
					<label for="user-name">Middle Name</label>
					<form:input id="user-name" 
							cssClass="form-control" path="mName"/>
				</div>
				<div class="form-group">
					<label for="user-name">Last Name</label>
					<form:input id="user-name" 
							cssClass="form-control" path="lName"/>
				</div>
				<div class="form-group">
					<label for="user-gender">Gender</label>
					<form:select items="${gender}" 
							cssClass="selectpicker" path="gender"></form:select>
				</div>
				<div class="form-group">
					<label for="user-type">Type</label>
					<form:select items="${typeOptions}" 
							cssClass="selectpicker" path="type"></form:select>
				</div>
				<div class="form-group">
					<label for="user-zoner">Zoner</label>
					<form:input id="user-zoner" 
							cssClass="form-control" path="zoner"/>
				</div>
				<div class="form-group">
					<label for="user-keywords">Keywords</label>
					<form:input id="user-keywords" 
							cssClass="form-control" path="keywords"/>
				</div>
				<div class="form-group">
					<label for="user-location">Location</label>
					<form:select  items="${locationOptions}"
							cssClass="selectpicker" path="location"/>
				</div>
				
				<div class="form-group">
					<label for="user-email">Email</label>
					<form:input path="email" cssClass="form-control" id="user-email"/>
				</div>
				
				<div class="form-group">
					<label for="user-ID">UserID</label>
					<form:input id="user-ID" 
							cssClass="form-control" path="userId"/>
							
				</div><div class="form-group">
					<label for="user-password">Password</label>
					<form:password id="user-password" 
							cssClass="form-control" path="password"/>
							
				</div><div class="form-group">
					<label for="user-number">Contact Number</label>
					<form:input id="user-Number" 
							cssClass="form-control" path="phoneNumber"/>
				</div>
				
				<button type="submit" class="btn btn-default">Submit</button>
	
			</form:form>
			
		</div>
	</div>
</body>
</html>