<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Artcon</title>

	<!-- jQuery -->
	<script src="<spring:url value="/resources/jquery/jquery.min.js"/>"></script>

    <!-- Bootstrap Core JavaScript -->
	<script src="<spring:url value="/resources/js/bootstrap.min.js"/>"></script>
    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<spring:url value="/resources/scrollreveal/scrollreveal.min.js"/>"></script>
    <script src="<spring:url value="/resources/magnific-popup/jquery.magnific-popup.min.js"/>"></script>

    <!-- Theme JavaScript -->
    <script src="<spring:url value="/resources/js/creative.min.js"/>"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<spring:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<spring:url value="/resources/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="<spring:url value="/resources/magnific-popup/magnific-popup.css"/>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<style>
nav{
    color: #2f2f2f;
}
.navbar-default{

}
.navbar-center{
    position: absolute;
    width: 100%;
    left: 0;
    text-align: center;
    margin:0 auto;
    font-size: 14px;
    font-weight: bold;
    font-family: 'Open Sans','Helvetica Neue',Arial,sans-serif;
}

.navbar-center li {
    display: inline!important;
    height: 50px;
    padding-top: 15px;
    padding-bottom:  15px;
    line-height: 50px;
}

.navbar-center>li>a {
    color: #2f2f2f;
    position: relative;
    display: inline;
    padding: 10px 15px;
    width: 150px;
}


#main-dropdown-menu{
    margin-top: 50px;
    padding-top: 20px;
    padding-bottom: 20px;
    background: #f8f8f8;
    border-top:1px solid black;
}

.main-search-menu{
    color: #2f2f2f;
    background: white;
    border:1px solid black;
    font-size: 16px;
    padding-left: 35px;
    padding-right: 35px;
    padding-top: 10px;
    padding-bottom: 10px; 
}

.cover-container{
    position: relative;
}
#cover-image-holder{
    padding-left: 10%;
    padding-right: 10%;
    max-height: 650px;
}



#cover-header{
    font-size: 75px;
    font-weight: bold;
    color: black;
}

#cover-detail{
    font-size: 25px;
    color: black;
}

#cover-sign-up{
    background: red;
    border:1px solid black;
    color: white;
    opacity: 1;
}

#big-menus{
    padding-bottom: 50px;
}
#cover-discover{
    background: white;
    border:1px solid black;
}

#cover-text-holder{
    width: 100%;
    height: 100%; 
    position: absolute;
    padding-left: 10%;
    padding-right: 10%;
    padding-top: 5%;
    top: 0;
    left: 0;
    background: white;
    opacity: 0.5;
}

.menu-box{
    border-top: 2px solid black;
}





#contact{
    background: black;
    color: white;
}

.contact-social>li{
    display: inline-block;
}
</style>
<body>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">ArtCon</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="navbar-center">
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <section id="main-dropdown-menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 col-lg-offset-1 text-center">
                    <div class="dropdown">
                        <button class="main-search-menu dropdown-toggle" type="button" data-toggle="dropdown">Name
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Artist 1</a></li>
                            <li><a href="#">Artist 2</a></li>
                            <li><a href="#">Artist 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="dropdown">
                        <button class="main-search-menu dropdown-toggle" type="button" data-toggle="dropdown">Artist
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Artist 1</a></li>
                            <li><a href="#">Artist 2</a></li>
                            <li><a href="#">Artist 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="dropdown">
                        <button class="main-search-menu dropdown-toggle" type="button" data-toggle="dropdown">Stand Up
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Artist 1</a></li>
                            <li><a href="#">Artist 2</a></li>
                            <li><a href="#">Artist 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="dropdown">
                        <button class="main-search-menu dropdown-toggle" type="button" data-toggle="dropdown">Genre
                        <span class="caret"></span></button>
                         <ul class="dropdown-menu">
                            <li><a href="#">Artist 1</a></li>
                            <li><a href="#">Artist 2</a></li>
                            <li><a href="#">Artist 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <button class="main-search-menu">Search</button>
                </div>
            </div>
        </div>
    </section>
    <section id="cover">
        <div class="container-fluid cover-container">
            <div id="cover-image-holder">
                <img width="100%" height="650px" src="<spring:url value="/resources/img/header.jpg"/>">
            </div>
            <div id="cover-text-holder">
                <div class="container">
                    <div class="row">
                        <button class="btn" id="cover-sign-up">Sign Up Free &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        <button class="btn" id="cover-discover">Discover &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                    </div>
                    <div class="row">
                        <div id="cover-header">We Give You <br/>your Voice</div>
                    </div>
                    <div id="cover-detail" class="row">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="big-menus">
    
        <div class="container">
            <div class="row">
                <hr class="primary">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="menu-box">
                        <h3>Musicians </h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="menu-box">
                        <h3>Poets</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="menu-box">
                        <h3>Stand Up</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="menu-box">
                        <h3>Threatre </h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contents">
        <div class="container">
            <div class="row content-box">
                <div class="col-md-6 content-image-box"><img width="100%" height="100%" src="<spring:url value="/resources/img/portfolio/fullsize/4.jpg"/>"></div>
                <div class="col-md-6 content-text-box content-text-box-right">
                    <div class="content-text-box-title"><h2>Hire Artist</h2></div>
                    <div class="content-text-box-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    <div class="content-text-box-button"></div>
                </div>
            </div>
            <div class="row content-box">
                <div class="col-md-6 content-text-box content-text-box-left">
                    <div class="content-text-box-title">Follow Employeers</div>
                    <div class="content-text-box-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    <div class="content-text-box-button"></div>
                </div>
                <div class="col-md-6 content-image-box"><img width="100%" height="100%"src="<spring:url value="/resources/img/portfolio/fullsize/1.jpg"/>"></div>
            </div>
            <div class="row content-box">
                <div class="col-md-6 content-image-box"><img width="100%" height="100%" src="<spring:url value="/resources/img/portfolio/fullsize/2.jpg"/>"></div>
                <div class="col-md-6 content-text-box content-text-right">
                    <div class="content-text-box-title">OnGoing Auditions</div>
                    <div class="content-text-box-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    <div class="content-text-box-button"></div>
                </div>
            </div>
            <div class="row content-box">
                <div class="col-md-6 content-text-box content-text-box-left">
                    <div class="content-text-box-title">Upcoming Events</div>
                    <div class="content-text-box-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    <div class="content-text-box-button"></div>
                </div>
                <div class="col-md-6 content-image-box"><img width="100%" height="100%" src="<spring:url value="/resources/img/portfolio/fullsize/3.jpg"/>"></div>
            </div>
        </div>
    </section>
    <section id="contents">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h1>Trending 1</h1>
                </div>
                <div class="col-md-3">
                    <h1>Treending 2</h1>
                </div>
                <div class="col-md-3">
                    <h1>Trending 3</h1>
                </div>
                <div class="col-md-3">
                    <h1>Trending 4</h1>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>LET'S CONNECT</h2>
                </div>
                <div class="col-md-12 text-center">
                    <ul class="contact-social">
                        <li><i class="fa fa-envelope-o fa-3x sr-contact"></i></li>
                        <li><i class="fa fa-envelope-o fa-3x sr-contact"></i></li>
                        <li><i class="fa fa-envelope-o fa-3x sr-contact"></i></li>
                        <li><i class="fa fa-envelope-o fa-3x sr-contact"></i></li>
                    </ul>
                </div>
                <div class="col-md-12 text-center">
                   
                </div>
            </div>
        </div>
    </section>

    
</body>

</html>
